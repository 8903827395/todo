import React from 'react';
import {appStore, getNotes, getTodoLists} from '../TodoStore/TodoAppStore';
import {Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import FaTrashO from 'react-icons/lib/fa/trash-o';

export class Note extends React.Component {
  constructor(props) {
    super(props);
    this.state = {modal: appStore.modal};
  }

  PopulateNote = () => {
    const notes = getNotes(appStore.currentTodoId, appStore.currentTaskId);
    let comments = [];
    for(var field in notes) {
      comments.push(
        <div className="comment-content" key={notes[field]}>
          <input type="textbox" className="comment-name" value={notes[field]} readOnly />
          <span className="FaTrashO">
            <FaTrashO />
          </span>
        </div>
      );
    }
    return(
      <div className="comment">
        {comments}
      </div>
    );
  }

  componentWillMount() {
    appStore.on('createNote', notes => {
      this.setState({notes: notes})
    })
    appStore.on('note-popup', ([modal, todoId, todoContent]) => {
      appStore.currentTodoContent = todoContent;
      this.setState({
        modal,
        currentTodoId: todoId,
        currentTodoContent: appStore.currentTodoContent
      });
    });
  }

  toggle = () => {
    appStore.modal = !appStore.modal;
    this.setState({
      modal: appStore.modal
    });
  }

  addComment = e => {
    if (e.key === 'Enter') {
      const todoLists = getTodoLists(appStore.currentTaskId);
      for(let i in todoLists) {
        if(todoLists[i].todoId === appStore.currentTodoId) {
          const notes = getNotes(appStore.currentTodoId, appStore.currentTaskId);
          const key = 'comment' + (new Date()).getTime();
          notes[key] = e.target.value;
          appStore.notes = notes;
        }
      }
      e.target.value = "";
      appStore.emit('createNote', appStore.notes);
    }
  }

  render() {
    return (
      <div>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>{ appStore.currentTodoContent }</ModalHeader>
          <ModalBody>
            <div className="scratch-notes">
              <div className="add-comment">
                <input className="add-comment-input" type="textbox" placeholder="Add a Comment..." onKeyUp={this.addComment} />
              </div>
            </div>
            <this.PopulateNote />
          </ModalBody>
          <ModalFooter>
            {/* <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '} */}
            {/* <Button color="secondary" onClick={this.toggle}>Cancel</Button> */}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
