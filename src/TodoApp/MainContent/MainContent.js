import React, {Component} from 'react';
import _ from 'underscore';
import {FaTrash} from 'react-icons/lib/fa';
import {appStore, getTodoLists} from '../TodoStore/TodoAppStore';
import * as TodoAction from '../actions/TodoAction';

export class MainContent extends Component {
  componentWillMount() {
    appStore.on('createTodo', todoLists => {
      let taskLists = appStore.taskLists;
      _.each(taskLists, () => {
        if (taskLists.taskId === appStore.currentTaskId) {
          taskLists.todo = todoLists;
        }
      });
      this.setState({currentTodoLists: todoLists});
    })
    appStore.on('Task-Switched', taskId => {
      this.setState({currentTaskId: taskId});
    });
    appStore.on('Remove-Task', () => {
      appStore.currentTaskId = "task1";
      this.setState({ currentTaskId: "task1" });
    });
  }

  onClick = (todo) => {
    TodoAction.createNotes(todo.todoId, todo.todoContent);
  }

  addTodo = e => {
    if (e.key === 'Enter') {
      var todoLists = getTodoLists(appStore.currentTaskId);
      var newTodoObj = {
        todoId: 'todo' + (new Date()).getTime(),
        todoContent: e.target.value,
        note: {}
      }
      todoLists.push(newTodoObj);
      e.target.value = "";
      TodoAction.createTodo(todoLists);
    }
  }

  removeTodo = (todo, taskId) => {
    const todoLists = getTodoLists(taskId);
    for (let i in todoLists) {
      if (todoLists[i].todoId === todo.todoId) {
        todoLists.splice(i, 1);
      }
    }
    this.setState({
      todoLists: todoLists
    });
  }

  PopulateTodoList = () => {
    const todoLists = getTodoLists(appStore.currentTaskId);
    const todos = todoLists.map((todo) =>
      <div className="todo-main" key={todo.todoId}>
      <div className = "note"  id={todo.todoId} onClick={() => this.onClick(todo)}>
        <div className="note-content">
        <input type="textbox" className="note-name" value={todo.todoContent} readOnly />
        </div>
      </div>
      <span className="note-icons" onClick={() => this.removeTodo(todo, appStore.currentTaskId)}>
        <FaTrash />
      </span>
    </div>
    );
    return (
      <div className="todo" >
        <div className="add-note">
          <input className="addNote-input" type="textbox" placeholder="Add a note..." onKeyUp={this.addTodo} />
        </div>
        <div>
          {todos}
        </div>
      </div>
    );
  }
  
  render() {
    return(
      <div className="main">
        <this.PopulateTodoList />
      </div>
    );
  }
}
