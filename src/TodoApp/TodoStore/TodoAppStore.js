import EventEmitter from 'eventemitter3';
import TodoDispatcher from '../dispatcher/TodoDispatcher';
import {Component} from 'react';

const taskLists = [
  {
  taskId: 'task1', 
  taskContent: 'Today',
  todo:[]
  }
];

export const getTaskLists = () => taskLists;

export const getTodoLists = task => {
  const taskLists = getTaskLists();
  let todoLists;
  for (let i in taskLists) {
    if (taskLists[i].taskId === task) {
      todoLists = taskLists[i].todo;
    }
  }
  return todoLists;
}

export const getNotes = (todoId, taskId) => {
  const todoLists = getTodoLists(taskId);
  let notes;
  for (let i in todoLists) {
    if (todoLists[i].todoId === todoId) {
      notes = todoLists[i].note;
    }
  }
  return notes;
}

class AppStore extends Component {
  constructor(props) {
    super(props);
    this.toggle = true;
    this.eventEmitter = new EventEmitter();
    this.modal = false;
    this.taskLists = taskLists;
    this.todoLists = [];
    this.notes = {};
    this.currentTaskId = "task1";
    this.currentTodoId = "";
    this.currentTodoContent = "";
  }

  on(eventName, listener) {
    this.eventEmitter.on(eventName, listener);
  }

  removeEventListener(eventName, listener) {
    this.eventEmitter.removeListener(eventName, listener);
  }

  emit(event, payload) {
    this.eventEmitter.emit(event, payload);
  }

  once(eventName, listener) {
    this.eventEmitter.once(eventName, listener);
  }

  getEventEmitter() {
    return this.eventEmitter;
  }

  handleAction = action => {
    switch(action.type) {
      case "SIDEBAR_TOGGLE" :
        this.toggle = !this.toggle;
        this.emit('event', this.toggle);
        break;
      case "SWITCH_TASK" :
        this.currentTaskId = action.taskId;
        this.emit('Task-Switched', this.currentTaskId);
        break;
      case "CREATE_TASK":
        this.emit('createTask', action.taskLists);
        break;
      case "CREATE_TODO":
        this.emit('createTodo', action.todoLists);
        break;
      case "CREATE_NOTES":
        this.modal = !this.modal;
        this.currentTodoId = action.todoId;
        this.emit('note-popup', [this.modal, action.todoId, action.todoContent]);
        break;
      default :
        break;
      // case "GET_TASKLISTS":
      //   this.emit('createTodo', action.todoLists);
      //   break;
    }
  }
}

export const appStore = new AppStore();

TodoDispatcher.register(appStore.handleAction.bind(appStore));
