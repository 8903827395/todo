import React, {Component} from 'react';
import './_TodoApp.scss';
import {AppHeader} from './TodoAppHeader/TodoAppHeader';
import {Sidebar} from './Sidebar/Sidebar';
import {MainContent} from './MainContent/MainContent';
import {Note} from './Modal/Modal';

export class TodoApp extends Component {
  render() {
    return (
      <div className="App">
        <header>
          <AppHeader />
        </header>
        <Sidebar />
				<MainContent />
				<Note />
			</div>
		);
	}
}
