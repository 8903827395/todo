import React from 'react';
import {FaPlus, FaList, FaTrashO} from 'react-icons/lib/fa';
import {appStore, getTaskLists, getTodoLists} from '../TodoStore/TodoAppStore';
import * as TodoAction from '../actions/TodoAction';

export class Sidebar extends React.Component {
  constructor(props) {
    super();
    this.state = {visible: true};
  }

  componentWillMount() {
    appStore.on('createTask', taskLists => this.setState({taskLists: taskLists}));
    appStore.on('event', toggle => this.setState({visible: toggle}));
    appStore.on('Remove-Task', this.removeTask);
  }

  onClick(e) {
    TodoAction.switchTask(e.currentTarget.id);
  }

  handleClick(e) {
    appStore.emit('Remove-Task', e.currentTarget.id);
  }

  removeTask = task => {
    const taskLists = getTaskLists();
    appStore.todoLists = getTodoLists("task1");

    for (let i in taskLists) {
      if (taskLists[i].taskId === task) {
        taskLists.splice(i, 1);
      }
    }
    this.setState({
      taskLists,
      currentTaskId: appStore.currentTaskId,
      currentTodoId: "",
      todoLists: appStore.todoLists
    });
  }

  PopulateSidebar = () => {
    const taskLists = getTaskLists();
    const tasks = taskLists.map(task =>
      <div className="tasks" key={task.taskId}>
        <div className="task"  id={task.taskId} onClick={this.onClick}>
          <span className='icon'>
            <FaList />
          </span>
          <span className="taskContent">{task.taskContent}</span>
        </div>
        { (task.taskId !== 'task1') &&
        <div className='delete' id={task.taskId} onClick={this.handleClick}>
          <FaTrashO />
        </div>
        }
      </div>
    );
    return (
      <div className="sidebar-content">
        {tasks}
      </div>
    );
  }

  addTask = e => {
    if ( e.key === 'Enter') {
      var newTaskObj = {taskId: 'task' + (new Date()).getTime(),
        taskContent: e.target.value, todo: []}
      var taskLists = appStore.taskLists;
      taskLists.push(newTaskObj);
      e.target.value = "";
      TodoAction.createTask(taskLists);
    }
  }

  render() {
    return (
      <div className={this.state.visible ? 'fadeIn' : 'fadeOut'}>
        <div className='sidebar'>
          <div className='newTask'>
            <span className='icon'>
              <FaPlus />
            </span>
            <input className="newTask-input" placeholder="Create a New Task"  onKeyUp={this.addTask }/>
          </div>
          <this.PopulateSidebar />
        </div>
      </div>
    );
  }
}