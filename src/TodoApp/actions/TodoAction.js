import TodoDispatcher from '../dispatcher/TodoDispatcher';

export function toggleSidebar() {
  TodoDispatcher.dispatch({
    type: "SIDEBAR_TOGGLE"
  });
}

export function switchTask(taskId) {
  TodoDispatcher.dispatch({
    type: "SWITCH_TASK",
    taskId
  });
}

export function createTask(taskLists) {
  TodoDispatcher.dispatch({
    type: "CREATE_TASK",
    taskLists
  });
}

export function createTodo(todoLists) {
  TodoDispatcher.dispatch({
    type: "CREATE_TODO",
    todoLists
  });
}

export function createNotes(todoId, todoContent) {
  TodoDispatcher.dispatch({
    type: "CREATE_NOTES",
    todoId,
    todoContent
  });
}

export function getTaskLists() {
  TodoDispatcher.dispatch({
    type: "GET_TASKLISTS"
  });
}