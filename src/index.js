import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { TodoApp } from './TodoApp/TodoApp';

ReactDOM.render(
    <TodoApp />,
    document.getElementById('root')
);

registerServiceWorker();
